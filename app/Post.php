<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = ['title', 'body', 'user_id'];

    public function user() {
        return $this->belongsTo('App\User');
        // Used to create relationship with users
    }
}
