<?php

namespace App\Http\Controllers;

use App\Post;
use Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct() {

        $this->middleware(['auth'])->except(['index', 'show']);

     }

    public function index()
    {
        $posts = Post::orderBy('created_at', 'desc')->paginate(9);

        return view('posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => ['required'],
            'body' => ['required'],
            'images' => ['required', 'max:2048']
        ]);

        $post = new Post;
        $post->title = $request->input('title');
        $post->body = $request->input('body');
        $post->user_id = auth()->id();

        if ($request->hasFile('images')) {
            $imgName = date('Y-m-d'). "_" . 
            $request->file('images')->getClientOriginalName();
            $post->images = $imgName;
        }

        if ($post->save()) {
            
            $request->images->move(public_path('images'), $imgName);
            return response()->json([
                'status' => 200,
                'sms' => 'Successfully created!!'
            ]);
        } else {
            return response()->json([
                'status' => 500,
                'sms' => 'Something went wrong!!'
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        
        return view('posts.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {

        if (Gate::denies('editpost', $post)) {  
            abort (404);
        }
        return view('posts.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        if (Gate::denies('editpost', $post)) {

            return back()->with('error', 'Access to this page is denied.');
        }

        if (! request()->ajax() && 
        !request()->isSecure()) {
            return response()->json(['error' => 'Failed to update.'], 500);
        }

        $request->validate([
            'title' => ['required'],
            'body' => ['required']
        ]);

        // $post = update(request([
        //     'title',
        //     'body'
        // ]));

        $post->update(request([
            'title',
            'body'
        ]));


        if ($post) {

            return response()->json([
                'status' => 200,
                'sms' => 'Successfully Edited!!'
            ]);
        } else {
            return response()->json([
                'status' => 500,
                'sms' => 'Something went wrong!!'
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        if (Gate::denies('editpost', $post)) {
            return back()->with('error', 'Access to this page is denied.');     
        }
        
        $post->delete();

        return redirect()->route('posts.index');
    }
}
