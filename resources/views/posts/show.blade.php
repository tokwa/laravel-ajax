@extends('layouts.app')

@section('content')
<h1>This is show page.</h1>
<div class="container">
    <div class="row">
        <div class="col-lg">
            <div class="card" style="width: 18rem;">
                <div class="card-body">
                    <h5 class="card-title">{{$post->title}}</h5>
                    <p class="card-text">{{$post->body}}</p>
                    <a href="{{URL::previous()}}" class="btn btn-primary">Back</a>
                    @can('editpost', $post)
                    <a href="{{route('posts.edit', $post->id)}}" class="btn btn-success">Edit</a>
                    @endcan
                    <br><br>
                    @can('editpost', $post)
                    <form method="POST" action="{{ route('posts.destroy', $post->id) }}">
                            @method('DELETE')
                            @csrf
                            <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                    @endcan
                </div>
            </div>
        </div>
    </div>
</div>
@endsection