@extends('layouts.app')

@section('content')
<h1>This is index</h1>
<div class="container">
    <div class="row">
    @if(count($posts))
        @foreach($posts as $post)
            <div class="col-lg-4 mb-5">
                <div class="card" style="width: 18rem;">
                    <div class="card-body">
                        <h5 class="card-title">{{$post->title}}</h5>
                        <br>
                        <small>Created at: {{$post->created_at}}</small>
                        <br>
                        <small>By: {{$post->user->name}}</small>
                        <img src="{{asset('images/' . $post->images)}}" alt="Post Image">
                        <p class="card-text">{{$post->body}}</p>
                        <a href="{{route('posts.show', $post->id)}}" class="card-link">View Post</a>
                    </div>
                </div>
            </div>
        @endforeach
    @else
        <p>
            There is no post at the moment.
        </p>
    @endif
    </div>
</div>
@endsection