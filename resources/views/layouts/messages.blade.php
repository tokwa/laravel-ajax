{{-- @if(count($errors) > 0)
    @foreach($errors->all() as $error)
    <div class="container">
        <div class="alert alert-danger">
            {{$error}}
        </div>
    </div>     
    @endforeach
@endif --}}


@if(session('success'))
    <div class="container-fluid">
        <div class="alert alert-success">
            {{session('success')}}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
@endif

@if(session('error'))
<div class="container-fluid">
    <div class="alert alert-danger">
        {{session('error')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
        </button>
    </div>
</div>
@endif